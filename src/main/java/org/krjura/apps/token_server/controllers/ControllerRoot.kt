package org.krjura.apps.token_server.controllers

import io.vertx.core.Vertx
import io.vertx.ext.web.Router
import org.krjura.apps.token_server.config.ApplicationConfig
import org.krjura.apps.token_server.controllers.oauth2.OAuthController

class ControllerRoot(
    private val vertx: Vertx, private val httpRouter: Router, private val config: ApplicationConfig) {

    fun configure() {
        OAuthController(vertx, httpRouter, config).configure()
    }
}