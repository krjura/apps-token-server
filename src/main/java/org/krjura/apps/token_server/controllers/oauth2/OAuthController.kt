package org.krjura.apps.token_server.controllers.oauth2

import io.vertx.core.Vertx
import io.vertx.ext.web.Router
import org.krjura.apps.token_server.config.ApplicationConfig

class OAuthController(
    private val vertx: Vertx, private val httpRouter: Router, private val config: ApplicationConfig) {

    fun configure() {
        OAuth2GoogleController(vertx, httpRouter, config).configure();
    }
}