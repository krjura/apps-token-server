package org.krjura.apps.token_server.controllers.oauth2

import io.vertx.core.Future
import io.vertx.core.Vertx
import io.vertx.core.http.Cookie
import io.vertx.core.http.CookieSameSite
import io.vertx.core.json.JsonObject
import io.vertx.ext.auth.User
import io.vertx.ext.auth.oauth2.OAuth2Auth
import io.vertx.ext.auth.oauth2.OAuth2FlowType
import io.vertx.ext.auth.oauth2.OAuth2Options
import io.vertx.ext.web.Router
import io.vertx.ext.web.RoutingContext
import org.krjura.apps.token_server.config.ApplicationConfig
import org.krjura.apps.token_server.config.OAuth2Data
import org.krjura.apps.token_server.utils.UrlUtils
import java.util.UUID

class OAuth2GoogleController(
    private val vertx: Vertx,
    private val httpRouter: Router,
    private val config: ApplicationConfig) {

    private val googleOAuth2Config: OAuth2Data? = retrieveGoogleOAuthConfig()
    private val googleOAuth2Client: OAuth2Auth? = createGoogleOAuth2Client()

    fun configure() {
        configureOAuthGoogle()
        configureOAuthGoogleCallback()
    }

    private fun configureOAuthGoogle() {
        httpRouter
            .route()
            .path("${config.server.contextPath}/oauth/${OAUTH2_PROVIDER_GOOGLE}")
            .handler { ctx: RoutingContext ->
                processGoogleEntrypoint(ctx)
            }
    }

    private fun processGoogleEntrypoint(ctx: RoutingContext) {
        if (googleOAuth2Config == null || googleOAuth2Client == null) {
            generateUnknownProviderError(ctx, OAUTH2_PROVIDER_GOOGLE)
            return
        }

        val state = UUID.randomUUID().toString()
        val authorizationUri = googleOAuth2Client.authorizeURL(
            JsonObject()
                .put(OAUTH2_REDIRECT_URI, googleOAuth2Config.redirectUrl)
                .put(OAUTH2_SCOPE, OAUTH2_SCOPE_EMAIL)
                .put(OAUTH2_STATE, state)
        )

        ctx.addCookie(
            Cookie
                .cookie(config.server.cookiePrefix + "oauth2_state", state)
                .setPath(config.server.contextPath)
                .setSameSite(CookieSameSite.STRICT)
                .setHttpOnly(config.server.cookieHttpOnly)
                .setSecure(config.server.cookieSecure)
                .setMaxAge(300)
        )

        redirect(ctx, authorizationUri)
    }

    private fun configureOAuthGoogleCallback() {
        httpRouter
            .route()
            .path("${config.server.contextPath}/oauth/${OAUTH2_PROVIDER_GOOGLE}/callback")
            .handler { ctx: RoutingContext ->
                processGoogleCallback(ctx)
            }
    }

    private fun processGoogleCallback(ctx: RoutingContext) {
        if (googleOAuth2Config == null || googleOAuth2Client == null) {
            generateUnknownProviderError(ctx, OAUTH2_PROVIDER_GOOGLE)
            return
        }

        val tokenConfig = JsonObject()
            .put(OAUTH2_CODE, ctx.queryParam(OAUTH2_CODE)[0])
            .put(OAUTH2_REDIRECT_URI, googleOAuth2Config.redirectUrl)

        googleOAuth2Client
            .authenticate(tokenConfig)
            .compose { user: User -> retrieveUserInfo(user) }
            .compose { userInfo: JsonObject -> processUserInfo(ctx, userInfo) }
            .onFailure { err: Throwable -> generateOAuthFailureError(ctx, err) }
    }

    private fun retrieveUserInfo(user: User): Future<JsonObject> {
        return googleOAuth2Client!!.userInfo(user)
    }

    private fun processUserInfo(ctx: RoutingContext, info: JsonObject): Future<Void> {
        /*
          {
            "sub" : "<user id>",
            "picture" : "<image url>",
            "email" : "example@gmail.com",
            "email_verified" : true
          }
         */
        val email = info.getString("email")

        val response = ctx.response()
        response.putHeader("content-type", "text/plain")
        response.end(email)

        return Future.succeededFuture()
    }

    private fun generateOAuthFailureError(ctx: RoutingContext, err: Throwable) {
        val url = config.server.externalPath +
                config.server.contextPath +
                config.server.errorPath +
                "?reason=oauth.invalid.login&error=" + UrlUtils.encode(err.message)

        redirect(ctx, url)
    }

    private fun generateUnknownProviderError(ctx: RoutingContext, provider: String) {
        val url = config.server.externalPath +
                config.server.contextPath +
                config.server.errorPath +
                "?reason=oauth.invalid.provider&providerName=" + provider

        redirect(ctx, url)
    }

    private fun redirect(ctx: RoutingContext, url: String) {
        ctx
            .response()
            .putHeader("Location", url)
            .setStatusCode(302)
            .end()
    }

    private fun retrieveGoogleOAuthConfig(): OAuth2Data? {
        return config.oauth2.getType(OAUTH2_PROVIDER_GOOGLE)
    }

    private fun createGoogleOAuth2Client(): OAuth2Auth? {
        if (googleOAuth2Config == null) {
            return null
        }

        val credentials = OAuth2Options()
            .setFlow(OAuth2FlowType.AUTH_CODE)
            .setClientID(googleOAuth2Config.clientId)
            .setClientSecret(googleOAuth2Config.clientSecret)
            .setAuthorizationPath(googleOAuth2Config.authorizationPath)
            .setTokenPath(googleOAuth2Config.tokenPath)
            .setUserInfoPath(googleOAuth2Config.userInfoPath)

        return OAuth2Auth.create(vertx, credentials)
    }

    companion object {
        private const val OAUTH2_PROVIDER_GOOGLE = "google"
        private const val OAUTH2_STATE = "state"
        private const val OAUTH2_SCOPE = "scope"
        private const val OAUTH2_REDIRECT_URI = "redirect_uri"
        private const val OAUTH2_SCOPE_EMAIL = "email"
        const val OAUTH2_CODE = "code"
    }
}