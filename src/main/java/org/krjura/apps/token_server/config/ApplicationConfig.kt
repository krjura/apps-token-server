package org.krjura.apps.token_server.config

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
data class ApplicationConfig @JsonCreator constructor(
    @JsonProperty("server") val server: Server,
    @JsonProperty("oauth2") val oauth2: OAuth2
)

@JsonIgnoreProperties(ignoreUnknown = true)
data class Server @JsonCreator constructor(
    @JsonProperty("port") val port: Int,
    @JsonProperty("contextPath") val contextPath: String,
    @JsonProperty("externalPath") val externalPath: String,
    @JsonProperty("errorPath") val errorPath: String,
    @JsonProperty("cookiePrefix") val cookiePrefix: String,
    @JsonProperty("cookieHttpOnly") val cookieHttpOnly: Boolean,
    @JsonProperty("cookieSecure") val cookieSecure: Boolean
)

@JsonIgnoreProperties(ignoreUnknown = true)
data class OAuth2 @JsonCreator constructor(
    @JsonProperty("types") val types: Map<String, OAuth2Data>) {

    fun getType(type: String): OAuth2Data? {
        return types[type]
    }
}

@JsonIgnoreProperties(ignoreUnknown = true)
data class OAuth2Data @JsonCreator constructor(
    @JsonProperty("clientId") val clientId: String,
    @JsonProperty("clientSecret") val clientSecret: String,
    @JsonProperty("authorizationPath") val authorizationPath: String,
    @JsonProperty("tokenPath") val tokenPath: String,
    @JsonProperty("userInfoPath") val userInfoPath: String,
    @JsonProperty("redirectUrl") val redirectUrl: String
)