package org.krjura.apps.token_server.utils

import java.lang.management.ManagementFactory

object JvmUtils {

    @JvmStatic
    fun jvmUptime(): Long {
        return ManagementFactory.getRuntimeMXBean().uptime
    }
}