package org.krjura.apps.token_server.utils

import java.net.URLEncoder
import java.nio.charset.StandardCharsets

object UrlUtils {

    fun encode(value: String?): String {
        return URLEncoder.encode(value, StandardCharsets.UTF_8)
    }
}