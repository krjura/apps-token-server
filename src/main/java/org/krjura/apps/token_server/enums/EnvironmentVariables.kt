package org.krjura.apps.token_server.enums

object EnvironmentVariables {
    const val APPLICATION_CONFIG_LOCATION = "APPLICATION_CONFIG_LOCATION"
    const val DEFAULT_APPLICATION_CONFIG_LOCATION = "etc/config/application.json"
    const val APPLICATION_ADDITIONAL_CONFIG_LOCATION = "APPLICATION_ADDITIONAL_CONFIG_LOCATION"
}