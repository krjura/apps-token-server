package org.krjura.apps.token_server

import io.vertx.config.ConfigRetriever
import io.vertx.config.ConfigRetrieverOptions
import io.vertx.config.ConfigStoreOptions
import io.vertx.core.AsyncResult
import io.vertx.core.Vertx
import io.vertx.core.VertxOptions
import io.vertx.core.http.HttpServer
import io.vertx.core.http.HttpServerOptions
import io.vertx.core.json.JsonObject
import io.vertx.ext.web.Router
import org.krjura.apps.token_server.config.ApplicationConfig
import org.krjura.apps.token_server.controllers.ControllerRoot
import org.krjura.apps.token_server.enums.EnvironmentVariables
import org.krjura.apps.token_server.enums.EnvironmentVariables.APPLICATION_ADDITIONAL_CONFIG_LOCATION
import org.krjura.apps.token_server.enums.EnvironmentVariables.APPLICATION_CONFIG_LOCATION
import org.krjura.apps.token_server.enums.EnvironmentVariables.DEFAULT_APPLICATION_CONFIG_LOCATION
import org.krjura.apps.token_server.utils.JvmUtils.jvmUptime
import org.slf4j.LoggerFactory
import java.util.concurrent.CompletableFuture
import java.util.concurrent.Future
import kotlin.system.exitProcess

class TokenServerMain {

    private var httpServerStarted = false

    private lateinit var vertx: Vertx;
    private lateinit var config: ApplicationConfig
    private lateinit var httpServer: HttpServer
    private lateinit var httpRouter: Router
    private lateinit var controllerConfig: ControllerRoot

    @Throws(Exception::class)
    fun init() {
        logger.info("init started at {} ms", jvmUptime())

        val vertxOptions = VertxOptions()
        vertx = Vertx.vertx(vertxOptions)
        config = configureVertx(vertx).get()

        setupHttpServer()
    }

    private fun setupHttpServer() {
        val httpServerOptions = HttpServerOptions()
            .setPort(config.server.port)

        httpServer = vertx.createHttpServer(httpServerOptions)

        httpRouter = Router.router(vertx)

        controllerConfig = ControllerRoot(vertx, httpRouter, config)
        controllerConfig.configure()

        httpServer.requestHandler(httpRouter)
        httpServer
            .listen(config.server.port)
            .onComplete {
                logger.info("HTTP server started at: {} ms", jvmUptime())
                httpServerStarted = true
            }
    }

    private fun configureVertx(vertx: Vertx): Future<ApplicationConfig> {
        // standard configuration
        val configLocation = System
            .getenv()
            .getOrDefault(APPLICATION_CONFIG_LOCATION, DEFAULT_APPLICATION_CONFIG_LOCATION)

        val configFileStore = ConfigStoreOptions()
            .setType("file")
            .setFormat("json")
            .setConfig(JsonObject().put("path", configLocation))

        // options
        val configOptions = ConfigRetrieverOptions()
            .addStore(configFileStore)

        // secrets configuration
        val additionalConfigLocation = System.getenv()[APPLICATION_ADDITIONAL_CONFIG_LOCATION]

        if (additionalConfigLocation != null && additionalConfigLocation.isNotEmpty()) {
            val additionalConfigurationFileStore = ConfigStoreOptions()
                .setType("file")
                .setFormat("json")
                .setConfig(JsonObject().put("path", additionalConfigLocation))

            configOptions.addStore(additionalConfigurationFileStore)
        }

        val retriever = ConfigRetriever.create(vertx, configOptions)

        val cf = CompletableFuture<ApplicationConfig>()
        retriever.getConfig { ar: AsyncResult<JsonObject> ->
            if (ar.failed()) {
                logger.error("failed to configure vertx", ar.cause())
                vertx
                    .close()
                    .onComplete { exitProcess(1) }
            } else {
                cf.complete(ar.result().mapTo(ApplicationConfig::class.java))

                logger.info("Vert.x configuration retrieved at: {} ms", jvmUptime())
            }
        }

        return cf
    }

    companion object {
        private val logger = LoggerFactory.getLogger(TokenServerMain::class.java)

        @Throws(Exception::class)
        @JvmStatic
        fun main(args: Array<String>) {
            val main = TokenServerMain()
            main.init()
        }
    }
}