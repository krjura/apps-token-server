import org.gradle.api.tasks.testing.logging.TestExceptionFormat
import org.gradle.api.tasks.testing.logging.TestLogEvent as TLE
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import kotlin.collections.mutableSetOf

plugins {
    kotlin("jvm") version "1.5.0"
}

group = "org.krjura.apps"
version = "1.0"

java {
    sourceCompatibility = JavaVersion.VERSION_15
    targetCompatibility = JavaVersion.VERSION_15

    withSourcesJar()
}

// extra
val vertxVersion = "4.0.3"
val fasterxmlVersion = "2.12.2"
val slf4jVersion = "1.7.30"
val logbackVersion = "1.2.3"
val junitVersion = "5.7.1"

repositories {
    mavenCentral()
    maven {
        setUrl("https://kjurasovic-scw-maven2.s3-website.fr-par.scw.cloud/releases")
    }
}

dependencies {
    // kotlin
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8:1.5.0")

    implementation("org.krjura.apps:apps-common-jwt:1.0.12")

    implementation("io.vertx:vertx-core:$vertxVersion")
    implementation("io.vertx:vertx-config:$vertxVersion")
    implementation("io.vertx:vertx-auth-oauth2:$vertxVersion")
    implementation("io.vertx:vertx-web:$vertxVersion")

    // json
    implementation("com.fasterxml.jackson.core:jackson-databind:$fasterxmlVersion")

    // logging
    implementation("org.slf4j:slf4j-api:$slf4jVersion")
    implementation("org.slf4j:log4j-over-slf4j:$slf4jVersion")
    implementation("ch.qos.logback:logback-classic:$logbackVersion")

    testImplementation("org.junit.jupiter:junit-jupiter-api:$junitVersion")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:$junitVersion")
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "15"
    }
}

tasks.withType<Test> {
    useJUnitPlatform()
    testLogging {
        events = mutableSetOf(TLE.FAILED, TLE.PASSED, TLE.SKIPPED, TLE.STANDARD_OUT)
        exceptionFormat = TestExceptionFormat.FULL
        showExceptions = true
        showCauses = true
        showStackTraces = true

        info {
            events = mutableSetOf(TLE.FAILED, TLE.PASSED, TLE.SKIPPED, TLE.STANDARD_OUT)
            exceptionFormat = TestExceptionFormat.FULL
        }

        debug {
            events = mutableSetOf(TLE.FAILED, TLE.PASSED, TLE.SKIPPED, TLE.STANDARD_OUT)
            exceptionFormat = TestExceptionFormat.FULL
        }
    }
}